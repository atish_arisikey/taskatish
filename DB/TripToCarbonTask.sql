-- MySQL dump 10.17  Distrib 10.3.25-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: TripToCarbonTask
-- ------------------------------------------------------
-- Server version	10.3.25-MariaDB-0+deb10u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `CarbonFootprint`
--

DROP TABLE IF EXISTS `CarbonFootprint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CarbonFootprint` (
  `CarbonFootprintId` int(11) NOT NULL AUTO_INCREMENT,
  `Activity` int(11) DEFAULT NULL,
  `ActivityType` varchar(45) DEFAULT NULL,
  `FuelType` varchar(45) DEFAULT NULL,
  `Mode` varchar(45) DEFAULT NULL,
  `Country` varchar(45) DEFAULT NULL,
  `CarbonFootprint` varchar(45) DEFAULT NULL,
  `Date` date DEFAULT NULL,
  PRIMARY KEY (`CarbonFootprintId`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CarbonFootprint`
--

LOCK TABLES `CarbonFootprint` WRITE;
/*!40000 ALTER TABLE `CarbonFootprint` DISABLE KEYS */;
INSERT INTO `CarbonFootprint` VALUES (10,22222,'miles','0','dieselCar','gbr','6955.49','2020-11-24'),(11,324,'miles','0','taxi','usa','74.52','2020-11-24'),(12,4443,'fuel','aviationGasoline','dieselCar','gbr','37187.91','2020-11-24'),(13,875,'fuel','aviationGasoline','petrolCar','gbr','7323.75','2020-11-24'),(14,782,'miles','jetFuel','economyFlight','gbr','109.48','2020-11-24'),(15,99,'fuel','aviationGasoline','dieselCar','def','824.67','2020-11-24'),(16,7387,'miles','0','anyCar','gbr','2400.78','2020-11-24'),(17,999,'fuel','motorGasoline','anyFlight','def','8581.41','2020-11-24'),(18,8845,'fuel','diesel','dieselCar','usa','90307.45','2020-11-24'),(19,717,'fuel','aviationGasoline','firstclassFlight','usa','5958.27','2020-11-24');
/*!40000 ALTER TABLE `CarbonFootprint` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-24 13:10:28
