let restAdapter = new RestAdapter();
let stringUtil = new StringUtil();
let activityMiles = '';
let activityType = '';
let fuelType = '';
let mode = '';
let country = '';
let carbonFootprint = 0;

let hideShowDiv = (activityType) => {
    activityType = activityType;
    if (activityType === 'fuel') {
        $('#fuelTypeDiv').show(500);
    } else {
        $('#fuelTypeDiv').hide(500);
    }
}

(function() {
    $('#storeDataButton').hide();
    getList();
}());

function getList() {
    restAdapter.executeGET('get_list', '', function(response) {
        onSuccess(response);
    });
}

function onSuccess(response) {
    var jsonObject = JSON.parse(response);
    let srNo = 1;
    employeeData = jsonObject.data;
    rowsPerPage = jsonObject.data.length;
    $("#tableDetails tbody tr").remove();
    $.each(jsonObject.data, function(index, value) {
        let markup = '<tr>' +
            '<th scope="row">' + (parseInt(srNo)) + '</th>' +
            '<td>' + value.Activity + '</td>' +
            '<td>' + value.ActivityType + '</td>' +
            '<td>' + value.FuelType + '</td>' +
            '<td>' + value.Mode + '</td>' +
            '<td>' + value.Country + '</td>' +
            '<td>' + value.CarbonFootprint + '</td>' +
            '<td>' + value.Date + '</td>' +
            '</tr>';

        $("#tableDetails tbody").append(markup);
        srNo++;
    });
}

function validation() {
    activityMiles = stringUtil.getString($('#activityMiles').val());
    activityType = stringUtil.getString($('input[type=radio][name="activityType"]:checked').val());
    fuelType = stringUtil.getString($('#fuelType').val());
    mode = stringUtil.getString($('#mode').val());
    country = stringUtil.getString($('#country').val());

    if (activityMiles === '' || activityMiles === '0') {
        alert('Enter miles or fuel in gallons.');
        $('#activityMiles').focus();
        return false;
    } else if (activityType === '' || activityType === '0') {
        alert('Select US Gallons or Miles type.');
        $('#activityType').focus();
        return false;
    }
    if (activityType === 'fuel')
        if (fuelType === '' || fuelType === '0') {
            alert('Select fuel type.');
            $('#fuelType').focus();
            return false;
        }

    return true;
}

function getCarbonFootprint() {
    if (validation()) {

        restAdapter.executeGET('api/get_carbon_footprint', '?activity=' + activityMiles + '&activity_type=' + activityType + '&fuel_type=' + fuelType + '&mode=' + mode + '&country=' + country, function(response) {
            var jsonObject = JSON.parse(response);
            console.log(jsonObject);
            switch (jsonObject.status) {
                case 0:
                    carbonFootprint = jsonObject.data;
                    $('#carbonFootprintResult').html(carbonFootprint);
                    $('#storeDataButton').show();
                    break;

                case 1:
                    alert(jsonObject.message);
                    break;

                default:
                    alert('Something happened terrible!');
                    break;
            }
        });
    }
}

function storeData() {
    let formData = new FormData();

    formData.append('activity_miles', activityMiles);
    formData.append('activity_type', activityType);
    formData.append('fuel_type', fuelType);
    formData.append('mode', mode);
    formData.append('country', country);
    formData.append('carbon_footprint', carbonFootprint);

    restAdapter.executePOST('insert_data', formData, function(response) {

        var jsonObject = JSON.parse(response);
        if (jsonObject.status === 0) {
            alert(jsonObject.message);
            getList();
            $('#storeDataButton').hide();
            clearFileds();
        }
        if (jsonObject.status === 1) {
            alert(jsonObject.message);
            // setTimeout(() => {
            //     window.location.href = _base_url + 'padadhikari_list';
            // }, 2000);
        }
    });
}

function clearFileds() {
    $('#activityMiles').val('');
    $('input[name=activityType]').attr('checked', false);
    $('#fuelType').prop('selectedIndex', 0);
    $('#mode').prop('selectedIndex', 0);
    $('#country').prop('selectedIndex', 0);
    $('#storeDataButton').hide();
    $('#carbonFootprintResult').html('0');
}