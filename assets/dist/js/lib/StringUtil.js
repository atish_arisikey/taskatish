/*
@authour: Atish Chandole, on 10th July 2020
*/

class StringUtil {
    constructor() //Empty Constructor
        {}

    isObjectEmpty(obj) {
        for(var prop in obj) {
            if(obj.hasOwnProperty(prop))
                return true;
        }
        return false;
    }

    isObjectNotEmpty(obj) {
        for(var prop in obj) {
            if(obj.hasOwnProperty(prop))
                return true;
        }
        return false;
    }

    removeHtmlSpecialChars(text)
    {
        text = text.trim();
        text = text.replace(/&quot;/gm, "\"");
        text = text.replace(/&amp;/gm, "&");
        text = text.replace(/&lt;/gm, "<");
        text = text.replace(/&gt;/gm, ">");
        return text;
    }

    getString(text) {
        if (text === '' || text === null || text === ' ' || text === 'null' || text === 'undefined' || text === undefined || text === false) {
            return '';
        }
        text = text.trim();
        text = text.replace(" </p><br><br><p>", "");
        text = text.replace(" <p> </p>", "");
        text = text.replace("<p> </p>", "");
        text = text.replace("       ", "");
        text = text.replace("<br /><br /><br />", "");
        text = text.replace("<br /><br />", "");
        text = text.replace("<br><br><br>", "");
        text = text.replace("<br><br>", "");
        text = text.replace(/&#039;/gm, "\'");
        text = text.replace(/&#39;/gm, "\'");
        text = text.replace("<br />\n<br />\n<br />", "");
        text = text.replace("\n\n\n", "");
        text = text.replace(/[']/gm, "\\'");
        text = text.replace(/["]/gm, '\\"');
        return text;
    }

    strip_html_tags(str) {
        if ((str === null) || (str === '') || (str == 'undefined') || (str == undefined))
            return false;
        else
            str = str.toString();

        return str.replace(/(<([^>]+)>)/igm, "");
    }

    truncate(str, length, ending) {

        if (length == null) {
            length = 20;
        }
        if (ending == null) {
            ending = '<a style=\'display: inline-block;line-height: 12px;\'href=\'javascript:void(0)\' onclick=readMorePopup(\'' + escape(str) + '\')>...</a>';
        }
        if (str.length > length) {
            return str.substring(0, length).concat(ending);
        } else {
            return str;
        }
    };
}