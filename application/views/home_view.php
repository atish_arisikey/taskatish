<main role="main" class="container">

	<div class="row">
		<div class="col-md-6 offset-md-3">
			<div class="form-row form-group ">

				<div class="col-md-8">
					<input type="number" min='1' maxlength="8" class="form-control" id="activityMiles" placeholder="e.g 340">
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="activityType" id="activityGallons" value="fuel">
					<label class="form-check-label" for="activityGallons">US Gallons</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="activityType" id="ativityMiles" checked value="miles">
					<label class="form-check-label" for="ativityMiles">Miles</label>
				</div>
			</div>

			<div class="form-row form-group" id='fuelTypeDiv'>
				<div class="col-md-2">
					<label for="fuelType">Fuel Type</label>
				</div>
				<div class="col-md-10">
					<select id="fuelType" class="form-control">
						<option value='0'>Choose...</option>
						<option value='motorGasoline'>Motor Gasoline</option>
						<option value='diesel'>Diesel</option>
						<option value='aviationGasoline'>Aviation Gasoline</option>
						<option value='jetFuel'>Jet Fuel</option>
					</select>
				</div>
			</div>

			<div class="form-row form-group">
				<div class="col-md-2">
					<label for="mode">Mode</label>
				</div>
				<div class="col-md-10">
					<select id="mode" class="form-control">
						<option value='dieselCar'>dieselCar</option>
						<option value='petrolCar'>Petrol Car</option>
						<option value='anyCar'>Any Car</option>
						<option value='taxi'>Taxi (Default)</option>
						<option value='economyFlight'>Economy Flight</option>
						<option value='businessFlight'>Business Flight</option>
						<option value='firstclassFlight'>Firstclass Flight</option>
						<option value='anyFlight'>Any Flight</option>
						<option value='motorbike'>Motorbike</option>
						<option value='bus'>Bus</option>
						<option value='transitRail'>Transit Rail</option>
					</select>
				</div>
			</div>

			<div class="form-row form-group">
				<div class="col-md-2">
					<label for="country">Country</label>
				</div>
				<div class="col-md-10">
					<select id="country" class="form-control">
						<option value='usa'>United States</option>
						<option value='gbr'>United Kingdom</option>
						<option value='def'>Other (Default)</option>
					</select>
				</div>
			</div>
			<button type="submit" onclick="getCarbonFootprint()" class="btn btn-primary">Get Carbon Footprint</button>
		</div>
	</div>
	<div class="h3 text-center mt-3">
		<p class="h3">Carbon Footprint : <strong><span id="carbonFootprintResult">0</span></strong> kilograms</p>
		<button type="submit" id='storeDataButton' onclick="storeData()" class="btn btn-primary">Save</button>
	</div>
	<hr>

	<table class="table" id='tableDetails'>
		<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">Ativity</th>
				<th scope="col">Activity Type</th>
				<th scope="col">Fuel Type</th>
				<th scope="col">Mode</th>
				<th scope="col">Country</th>
				<th scope="col">Carbon Footprint (Kilograms)</th>
				<th scope="col">Date</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</main>

<script src="<?= ASSET_URL ?>js/page/home.js"></script>