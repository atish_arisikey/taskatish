<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <link rel="stylesheet" href="<?= ASSET_URL ?>css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style>
        /* Show it is fixed to the top */
        body {
            padding-top: 4.5rem;
        }
    </style>
    <script src="<?= ASSET_URL ?>js/jquery.min.js"></script>
    <script src="<?= ASSET_URL ?>js/bootstrap.min.js"></script>
    <script src="<?= ASSET_URL ?>js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script src="<?= ASSET_URL ?>js/lib/RestAdapter.js"></script>
    <script src="<?= ASSET_URL ?>js/lib/StringUtil.js"></script>

    <title><?= $page_title ?></title>
</head>

<body>