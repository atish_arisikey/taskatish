<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function index()
	{
		$data['page_title'] = 'Home';
		$data['page'] = 'home_view';
		$this->load->view('include/template', $data);
	}

	public function getCarbonFootprint()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		$response = [
			'status' => 1,
			'message' => 'Ooh! Somthing happened terrible.',
			'data' => '',
		];
		if ($method === "GET") {
			//get parameter from api
			$activity = $_GET['activity'];
			$activityType = $_GET['activity_type'];
			$fuelType = $_GET['fuel_type'];
			$mode = $_GET['mode'];
			$country = $_GET['country'];

			//---------curl--------------------
			$header = [
				'Content-Type: application/json'
			];

			$curl = curl_init();
			$url = "https://api.triptocarbon.xyz/v1/footprint";
			$urlQuery = "?activity=$activity&activityType=$activityType";//Require params

			if ($fuelType != '0' || $fuelType != '') {//Optional params
				if (!empty($urlQuery)) {
					$urlQuery .= "&";
				}
				$urlQuery .= "fuelType=$fuelType";
			}

			if ($mode != '0' || $mode != '') {//Optional params
				if (!empty($urlQuery)) {
					$urlQuery .= "&";
				}
				$urlQuery .= "mode=$mode";
			}

			if ($country != '0' || $country != '') {//Optional params
				if (!empty($urlQuery)) {
					$urlQuery .= "&";
				}
				$urlQuery .= "country=$country";
			}

			curl_setopt_array($curl, array(
				CURLOPT_URL => $url . $urlQuery,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_HTTPHEADER => $header
			));

			$distance = json_decode(curl_exec($curl));
			$err = json_decode(curl_error($curl));
			curl_close($curl);
			//---------\.curl--------------------

			if ($err) {
				$response = [
					'status' => 1,
					'message' => 'Error',
					'data' => $err
				];
			} else {
				if (isset($distance->carbonFootprint)) {
					$response = [
						'status' => 0,
						'message' => 'Success',
						'data' => $distance->carbonFootprint
					];
				} else {
					$response = [
						'status' => 1,
						'message' =>  $distance->errorMessage,
						'data' => null
					];
				}
			}
		} else {
			$response = [
				'status' => 1,
				'message' =>  'Request method cannot be accepted.',
				'data' => null,
			];
		}

		echo json_encode($response);
	}

	public function insertData()
	{
		$response = [
			'status' => 1,
			'message' => 'Ooh! Somthing happened terrible.',
			'data' => '',
		];

		$method = $_SERVER['REQUEST_METHOD'];
		if ($method === "POST") {

			$activityMiles = '';
			if (isset($_POST['activity_miles'])) {
				$activityMiles = $_POST['activity_miles'];
			}

			$activityType = "";
			if (isset($_POST['activity_type'])) {
				$activityType = $_POST['activity_type'];
			}

			$fuelType = "";
			if (isset($_POST['fuel_type'])) {
				$fuelType = $_POST['fuel_type'];
			}

			$mode = "";
			if (isset($_POST['mode'])) {
				$mode = $_POST['mode'];
			}

			$country = "";
			if (isset($_POST['country'])) {
				$country = $_POST['country'];
			}

			$carbonFootprint = "";
			if (isset($_POST['carbon_footprint'])) {
				$carbonFootprint = $_POST['carbon_footprint'];
			}


			$isInserted = $this->HomeModel->insertData($activityMiles, $activityType, $fuelType, $mode,  $country, $carbonFootprint);

			if ($isInserted) {
				$response = [
					'status' => 0,
					'message' => 'Added successfully!',
					'data' => $isInserted
				];
			} else {
				$response = [
					'status' => 1,
					'message' => 'Ooh! Data not added, try again later',
					'data' => $isInserted
				];
			}
		} else {
			$response = [
				'status' => 1,
				'message' =>  'Request method cannot be accepted.',
				'data' => null,
			];
		}

		echo json_encode($response);
	}

	public function getList()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method === "GET") {
			$response = [
				'status' => 1,
				'message' => 'Ooh! Somthing happened terrible.',
				'data' => '',
			];

			$data = $this->HomeModel->getList();

			if ($data) {
				$response = [
					'status' => 0,
					'message' => 'Success',
					'data' => $data
				];
			} else {
				$response = [
					'status' => 1,
					'message' => 'Record not found',
					'data' => null
				];
			}
		} else {
			$response = [
				'status' => 1,
				'message' =>  'Request method cannot be accepted.',
				'data' => null,
			];
		}

		echo json_encode($response);
	}
}
