<?php
class HomeModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insertData($activityMiles, $activityType, $fuelType, $mode,  $country,$carbonFootprint)
    {
        $currentDateTime = date("Y-m-d H:i:s");
        $sql = "Insert into CarbonFootprint(Activity,ActivityType,FuelType,Mode,Country,CarbonFootprint,Date)
                Values($activityMiles,'$activityType','$fuelType','$mode','$country','$carbonFootprint','$currentDateTime')";

        return $this->db->query($sql);
    }

    public function getList()
    {
        $sql = "select *
                FROM CarbonFootprint 
                order by CarbonFootprintId desc";

        return $this->db->query($sql)->result_array();
    }
}
